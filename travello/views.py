from travello.models import Destination
from django.shortcuts import render

# Create your views here.
def home(request):

    dest1 =Destination()
    dest1.name = "Anas Mourad"
    dest1.desc = "Hi there , I am a bot and this is the first django project for me"
    return render(request,'index.html', {'dest1' : dest1})
